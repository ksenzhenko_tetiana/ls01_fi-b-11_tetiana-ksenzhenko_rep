import java.util.Scanner;

public class Quersumme {

	public static void main(String[] args)
	{
	    int temp_zahl; 
	    int sum = 0;
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Dieses Programm berechnet die Quersumme einer vorgegebenen ganzen Zahl.");
	    temp_zahl = eingabe(myScanner, "Bitte geben Sie die Zahl ein: ");

	    do
	    {sum += temp_zahl % 10;
        temp_zahl /= 10;
	    }
	    while (temp_zahl > 0);
	    
	    System.out.println(sum);    
	     
	}
	
	public static int eingabe(Scanner ms, String i ) {
		System.out.print(i);
		int zahl = ms.nextInt();
		return zahl;
	}	
}
	
