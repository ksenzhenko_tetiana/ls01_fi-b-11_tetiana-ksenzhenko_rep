import java.util.Scanner;


public class PCHaendlerMitMethoden {

	public static void main(String[] args) {
		String artikel= "";
		
		Scanner myScanner = new Scanner(System.in);
		artikel = liesString("Was möchten Sie bestellen?", myScanner);
	    int anzahl = liesInt("Geben Sie die Anzahl ein:", myScanner);
	    double preis= liesDouble("Geben Sie den Nettopreis ein:", myScanner);
	    double mwst= liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:", myScanner);
	    double netto = berechneGesamtnettopreis(anzahl, preis);
	    double brutto = berechneGesamtbruttopreis(netto, mwst);
	    rechungausgeben(artikel, anzahl, netto, brutto,mwst);
        myScanner.close();
	}

	public static String liesString(String text, Scanner myScanner){
		//Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String artikel = myScanner.next();
		return artikel;
	}
	
	public static int liesInt(String text, Scanner myScanner) {
		System.out.println(text);
		int anzahl = myScanner.nextInt();
		return anzahl;
	}	
	
	public static double liesDouble(String text, Scanner myScanner) {
		System.out.println(text);
		double wert = myScanner.nextDouble();
		return wert;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double preis) {
		double nettogesamtpreis = anzahl * preis;
		return nettogesamtpreis;
		}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
		}
	
	public static void rechungausgeben(String artikel, int anzahl, double
			nettogesamtpreis, double bruttogesamtpreis,
			double mwst) {
		
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		
	}
}




