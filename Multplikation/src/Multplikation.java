import java.util.Scanner;

public class Multplikation {

	public static void main(String[] args) {
		 double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;
        
          
	        Scanner myScanner = new Scanner(System.in);
	        System.out.println("Hinweis: ");
	        System.out.println("Das Programm multipliziert 2 eingegebene Zahlen. ");
	        
			zahl1 = liesDouble("1. Zahl:", myScanner);
			zahl2 = liesDouble("2. Zahl:", myScanner);
			erg = verarbeitung(zahl1, zahl2);
	        ausgabe(zahl1, zahl2, erg);
			myScanner.close();

	    	}
	        
	        public static double liesDouble(String text, Scanner myScanner) {
	        	System.out.println(text);
	    		double zahl = myScanner.nextDouble();
	    		return zahl;
	    	}
	        
	        public static double verarbeitung(double zahl1, double zahl2) {
	    		double erg = zahl1 * zahl2;
	    		return erg;
	    	}
	        
	        public static void ausgabe(double zahl1, double zahl2, double erg) {
	    		
	        	System.out.println("Ergebnis der Multiplikation: ");
		        System.out.printf("%.2f * %.2f = %.2f%n", zahl1, zahl2, erg);
		   	}

	    
}
