import java.util.Scanner;

public class Gruss {

	public static void main(String[] args) {
		
		        System.out.println("Guten Tag, Damen und Herren!");
		        // Neues Scanner-Objekt myScanner wird erstellt
				 Scanner myScanner = new Scanner(System.in);

				 System.out.println("Bitte geben Sie Ihre Name und Vorname ein: ");

				 // Die Variable zahl1 speichert die erste Eingabe
				 String name = myScanner.next();

				 System.out.println("Bitte geben Sie Ihre Alter ein: ");

				 // Die Variable zahl2 speichert die zweite Eingabe
				 int alter = myScanner.nextInt();

				 // Die Addition der Variablen zahl1 und zahl2
				 // wird der Variable ergebnis zugewiesen.
				 //int ergebnis = name + alter;

				 System.out.print("\n\n\nSchön Sie zu sehen - ");
				 System.out.print(name + ", " + alter + " Jahre alt ");
				 myScanner.close();


	}

}
