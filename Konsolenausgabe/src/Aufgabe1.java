
public class Aufgabe1 {

	public static void main(String[] args) {
		
		//Übung
		
		//System.out.println("Hallo!\nWie geht es dir?");
        //System.out.println("\tHallo!\tHallo!");
        //System.out.println("\\Hallo!\\");
        
        //String z = "Java-Programm";
        //int n =123;
        //double k = 1234.5678;
        
        //System.out.printf( "\n|%s|\n", z );
        //System.out.printf( "|%20.4s|\n", z );
        
        //System.out.printf("\n|%+-15d| |%15d|", n, -n);
        
        //System.out.printf( "\n|%f| |%f|\n" , k, -k);
		
		//Aufgabe1
		System.out.println("Aufgabe1");
		System.out.println();
		String s = "Das ist der erste Satz.";
		String z = "Und der zweite kommt auch.";
		
		System.out.println("Das ist der erste Satz. Und der zweite kommt auch.");
		System.out.println("Das ist \'der erste Satz\'.\nUnd \'der zweite\' kommt auch.");
		
		System.out.printf( "\n|%-50s|\n|%50s|\n", s, z);
		//der Unterschied zwischen print() und println ist dass print() schreibt einfach alles was es unter "" gibt, und println() macht ein Einzug nach dem Text in "", so nächste Printausgabe fängt von der neuen Line an.
		System.out.println();
		System.out.println();
		System.out.println("Aufgabe2");
		System.out.println();
		
        //Aufgabe2
		
		
		String w = "*";
		String v = "*********";
		
		System.out.printf("%10s\n", w);
		System.out.printf("%9s%s%s\n", w, w, w);
		System.out.printf("%8s%s%s%s%s\n", w, w, w, w, w);
		System.out.printf("%7s%s%s%s%s%s%s\n", w, w, w, w, w, w, w);
		System.out.printf("%14s\n", v);
		System.out.printf("%13s%s%s\n", v, w, w);
		System.out.printf("%12s%s%s%s%s\n", v, w, w, w, w);
		System.out.printf("%9s%s%s\n", w, w, w);
		System.out.printf("%9s%s%s\n", w, w, w);
		
		System.out.println();
		System.out.println();
		System.out.println("Aufgabe3");
		System.out.println();
		
        //Aufgabe3
		
		double one = 22.4234234;
	    double two = 111.2222;
	    double three = 4.0;
	    double four = 1000000.551;
	    double five = 97.34;
	    
	    System.out.printf("%.2f\n", one);
	    System.out.printf("%.2f\n", two);
	    System.out.printf("%.2f\n", three);
	    System.out.printf("%.2f\n", four);
	    System.out.printf("%.2f\n", five);
		
		
        
		
	}

}
