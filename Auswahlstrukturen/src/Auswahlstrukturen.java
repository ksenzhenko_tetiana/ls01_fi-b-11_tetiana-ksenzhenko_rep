// Aufgabe 1 && Aufgabe 6
// https://ksenzhenko_tetiana@bitbucket.org/ksenzhenko_tetiana/ls01_fi-b-11_tetiana-ksenzhenko_rep.git

import java.util.Scanner;

public class Auswahlstrukturen {

	public static void main(String[] args) {
		Scanner ms = new Scanner(System.in);
		int i; // Aufgabe 1a
		int zahl1, zahl2; // Aufgabe 1b,c,d
		
		double x; // Aufgabe 6
		final float e =2.718f;
			
		System.out.println("Choose the number of the situation: \n1. If i have school \n2. If i have work");
		i =  ms.nextInt();
				
		if (i == 1)
		{ System.out.println("then I need to wake up at 6am.");}
		
		if (i == 2)
		{ System.out.println("then I need to wake up at 8am.");}
		
		System.out.print("\n\nEnter the first number: ");
		zahl1 =  ms.nextInt();
		
		System.out.print("\nEnter the second number: ");
		zahl2 =  ms.nextInt();
				
//		if (zahl1 == zahl2)
//		{ System.out.println("Number 1 equals Number 2. ");}
//		
//		if (zahl2 > zahl1)
//		{ System.out.println("Number 2 is bigger than Number 1. ");}
//		
		if (zahl1 >= zahl2)
		{ System.out.println("Number 1 equals or is bigger than Number 2. "); }
		
		else {System.out.println("Number 2 is bigger than Number 1");}
		
		
		// Aufgabe 6 
		
		System.out.print("\n\nEnter x for the function y=f(x) : ");
		x =  ms.nextInt();
		
		if (x <= 0)
		{ System.out.println("When x equals or is smaller than 0, then the function is exponential and ");
		  System.out.print("y = "+ Math.pow(e, x));}
		
		if (x <= 3 && x > 0)
		{ System.out.println("When x is (0;3], then the function is quadratic and ");
		  System.out.print("y = "+ (x*x+1));} 
		
		else if (x > 3) { System.out.println("When x is bigger than 3, then the function is lineal and ");
		       System.out.print("y = " + (2*x+4));}
	
	
	}

}
