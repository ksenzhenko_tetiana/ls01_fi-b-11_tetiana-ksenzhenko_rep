import java.util.Scanner;

public class MittelwertMitMethoden {

	public static void main(String[] args) {
		double zahl1;
		double zahl2;
		double m;
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Dieses Programm berechnet den Mittelwert zweier Zahlen.");
		zahl1 = eingabe(myScanner, "Bitte geben Sie die erste Zahl ein: ");
		
		zahl2 = eingabe(myScanner, "Bitte geben Sie die zweite Zahl ein: ");
		m = mittelwertBerechnung(zahl1, zahl2);
		
		ausgabe(m);
		
		myScanner.close();
	}
	
	public static double eingabe(Scanner ms, String text ) {
		System.out.print(text);
		double zahl = ms.nextDouble();
		return zahl;
	}
	
	public static double mittelwertBerechnung(double zahl1, double zahl2) {
		double m = (zahl1 + zahl2)/ 2.0;
		return m;
	}
	
	public static void ausgabe(double mittelwert) {
		System.out.println("Der errechnete Mittelwert beider Zahlen ist: " + mittelwert);
	}
}
