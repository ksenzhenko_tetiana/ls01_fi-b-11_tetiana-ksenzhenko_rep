import java.util.Scanner;

class Fahrkartenautomat1
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       float zuZahlenderBetrag; 
       float eingezahlterGesamtbetrag;
       float eingeworfeneMünze;
       float rückgabebetrag;
       int tickets;

              
       
       System.out.print("Ticketpreis (EURO): ");
       zuZahlenderBetrag = tastatur.nextFloat();
       
       System.out.print("Anzahl der Tickets: ");
       tickets = tastatur.nextInt();
       zuZahlenderBetrag *= tickets;

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0f;
       System.out.printf("Zu zahlender Betrag : %.2f Euro",  zuZahlenderBetrag );
       while(eingezahlterGesamtbetrag <  zuZahlenderBetrag)
       {
    	   System.out.printf("\n\nNoch zu zahlen: %.2f Euro ",  zuZahlenderBetrag - eingezahlterGesamtbetrag);
    	   System.out.print("\nEingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextFloat();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
           
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0f)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro", rückgabebetrag);
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(Math.round(rückgabebetrag*100.0)/100.0 >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       tastatur.close();
       
    }
}

//fahrkartenbestellungErfassen
//fahrkartenBezahlen
//fahrkartenAusgeben
//rueckgeldAusgeben