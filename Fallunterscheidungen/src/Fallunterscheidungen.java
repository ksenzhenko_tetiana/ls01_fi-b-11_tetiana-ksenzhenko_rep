import java.util.Scanner;

public class Fallunterscheidungen {

	public static void main(String[] args) {
		
		Scanner ms = new Scanner(System.in);
		byte note; // Aufgabe 1

		
		System.out.print("Geben Sie Ihre Note ein:  ");
		note = ms.nextByte();
	
		switch(note) {
		case 1: 
			System.out.println("Sehr gut");
			break;
		case 2: 
			System.out.println("Gut");
			break;
		case 3: 
			System.out.println("Befriedigend");
			break;
		case 4: 
			System.out.println("Ausreichend");
			break;
		case 5: 
			System.out.println("Mangelhaft");
			break;
		case 6: 
			System.out.println("Ungenügend");
			break;	
		}

		
		
	}

}
