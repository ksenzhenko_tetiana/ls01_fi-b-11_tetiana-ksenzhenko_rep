//repository https://ksenzhenko_tetiana@bitbucket.org/ksenzhenko_tetiana/ls01_fi-b-11_tetiana-ksenzhenko_rep.git

import java.util.Scanner;

public class FahrkartenautomatMethoden {

	
	    public static void main(String[] args)
	    
	    {
	       double zuZahlenderBetrag; 
	       double rückgabebetrag;
	       int time = 1000;
	       
	       zuZahlenderBetrag = fahrkartenbestellungErfassen();
	       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
//	       fahrkartenAusgeben();
	       warte(time); 
	       rueckgeldAusgeben(rückgabebetrag);      
	    }
	    
	    public static double fahrkartenbestellungErfassen() {
	    	
	    	Scanner tastatur = new Scanner(System.in);
	    	
	    	System.out.print("Ticketpreis (EURO): ");
	        double ticketpreis = tastatur.nextFloat();
	        
	        System.out.print("Anzahl der Tickets: ");
	        byte anzahltickets = tastatur.nextByte();
	        
	        double zuZahlenderBetrag = ticketpreis * anzahltickets;
	       
	        return zuZahlenderBetrag;
	    }
	    
	    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
	    	
	    	Scanner tastatur = new Scanner(System.in);
	    	double eingezahlterGesamtbetrag = 0.0f;
	        
	    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	        {
	     	   zuZahlenderBetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
	     	   
	     	   System.out.printf("Noch zu zahlen: %.2f Euro \n", zuZahlenderBetrag);
	     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	     	   
	     	   double eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	        }
	        double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	        
	        return rückgabebetrag;
	    }
	    
	        
//	    public static void fahrkartenAusgeben() {
//	    	
//	    }
	    
	    public static void warte(int millisekunde) {
	    	
	    	System.out.println("\nFahrschein wird ausgegeben");
	    	
	    	 	for (int i = 0; i < 8; i++)
	        {
	    		System.out.print("=");
	           try {
	 			Thread.sleep(millisekunde);
	 		} catch (InterruptedException e) {
	 			e.printStackTrace();}
	        }
	        System.out.println("\n\n");
	    }
	    
	    
	    public static void rueckgeldAusgeben(double rückgabebetrag) {
	    	
	    	if(rückgabebetrag > 0.0)
	         {
	      	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro \n", rückgabebetrag);
	      	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	             while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	             {
	          	  System.out.println("2 EURO");
	  	          rückgabebetrag -= 2.0;
	             }
	             while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	             {
	          	  System.out.println("1 EURO");
	  	          rückgabebetrag -= 1.0;
	             }
	             while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	             {
	          	  System.out.println("50 CENT");
	  	          rückgabebetrag -= 0.5;
	             }
	             while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	             {
	          	  System.out.println("20 CENT");
	   	          rückgabebetrag -= 0.2;
	             }
	             while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	             {
	          	  System.out.println("10 CENT");
	  	          rückgabebetrag -= 0.1;
	  	          
	             }
	             while(Math.round(rückgabebetrag*100.0)/100.0 >= 0.05)// 5 CENT-Münzen
	             {
	          	  System.out.println("5 CENT");
	   	          rückgabebetrag -= 0.05;
	             }
	         }

	         System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                            "vor Fahrtantritt entwerten zu lassen!\n"+
	                            "Wir wünschen Ihnen eine gute Fahrt.");
	    }
	}