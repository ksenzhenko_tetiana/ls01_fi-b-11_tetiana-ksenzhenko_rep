/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstraße
     double   anzahlSterne = 4E+11;
    
    // Wie viele Einwohner hat Berlin?
     int   bewohnerBerlin = 3645600;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
      short alterTage = 8550;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
      int  gewichtKilogramm =  150000;
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
      double  flaecheGroessteLand = 171E+8;
    
    // Wie groß ist das kleinste Land der Erde?
    
     float  flaecheKleinsteLand = 0.44f;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne:" + anzahlSterne);
    System.out.println("Berlin hat " + bewohnerBerlin + " Einbewohner");
    System.out.println("Ich bin " + alterTage + " Tagen alt");
    System.out.println("Das schwerste Tier der Welt wiegt " + gewichtKilogramm);
    System.out.println("Das größte Land der Erde hat " + flaecheGroessteLand + "km2" );
    System.out.println("Das kleinste Land der Erde ist " + flaecheKleinsteLand + "km2");
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
