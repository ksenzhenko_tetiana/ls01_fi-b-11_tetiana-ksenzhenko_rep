import java.util.Scanner;

public class MittelwertmitSchleife {

	public static void main(String[] args) {
		
		
			Scanner ms = new Scanner(System.in);
			
			System.out.print("Geben Sie die Anzahl der einzugebenden Werte ein: ");
			int anzahl = ms.nextInt();
			if (anzahl == 0)
			{
			    System.out.println("Kein Durchschnitt aus 0 Zahlen. Bitte versuchen Sie nochmal.");
			}
			else
			{
			    double temp_zahl = 0;
			    double gesamt = 0;
			    for (int i = 1; i < anzahl + 1; i++)
			    {
				System.out.print(i + ". Zahl = ");
				temp_zahl = ms.nextDouble();
				gesamt += temp_zahl;
			    }
			    System.out.println("Durchschnitt = " + gesamt / anzahl);
			 }
	}
}