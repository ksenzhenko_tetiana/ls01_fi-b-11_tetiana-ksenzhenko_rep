import java.util.Scanner;

public class FahrkartenautomatEndlosmodus {
	
	 public static void main(String[] args)
	    
	    {
	double zuZahlenderBetrag; 
    double rückgabebetrag;
    int time = 1000;
    
    zuZahlenderBetrag = fahrkartenbestellungErfassen();
    rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    warte(time); 
    rueckgeldAusgeben(rückgabebetrag);  
	    }
	 
		public static double fahrkartenbestellungErfassen() {
			int anzahlTickets;
			double ticketPreis = 0;
			int auswahl;
			Scanner tastatur = new Scanner(System.in);

			System.out.println("Fahrkartenbestellvorgang:");
		
			for(int i=0; i<25; i++) 
			{
				System.out.print("=");
			}
			System.out.println();
			System.out.printf("\nWählen Sie ihre Wunschfahrkarte für Berlin AB aus: \n\n");
			System.out.println("(1)  Einzelfahrschein Regeltarif AB [2,90 EUR] ");
			System.out.println("(2)  Tageskarte Regeltarif AB [8,60 EUR] ");
			System.out.println("(3)  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] )");
			System.out.printf("\nIhre Wahl: ");
			auswahl = tastatur.nextInt();
			  
		   
		    while ( auswahl >= 4 )
			{System.out.println(">>falsche Eingabe<<");
			System.out.println(">>Versuchen Sie nochmal<<");
			System.out.printf("\nIhre Wahl: ");
			auswahl = tastatur.nextInt();
						} 
		    
			if (auswahl == 1) 
			{
				System.out.println(); 
				ticketPreis = 2.90;
				
			}
			else if (auswahl == 2) 
			{
				System.out.println();  
				 ticketPreis = 8.60; 
			}
			else if (auswahl == 3) 
			{
				System.out.println();
				ticketPreis = 23.50;
			}				
				
			System.out.print("Anzahl der Tickets: ");
			anzahlTickets = tastatur.nextInt();
			
			if (anzahlTickets == 0)
			{ System.out.println("SIE HABEN DIE ANZAHL DER TICKETS NICHT EINGEGEBEN.\nVERSUCHEN SIE NOCH EINMAL!");
			System.exit(0);}
			
			else
				 System.out.println();
			
			return ticketPreis * anzahlTickets;
		}

	    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
	    	
	    	Scanner tastatur = new Scanner(System.in);
	    	double eingezahlterGesamtbetrag = 0.0f;
	        
	    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	        {
	     	   zuZahlenderBetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
	     	   
	     	   System.out.printf("Noch zu zahlen: %.2f Euro \n", zuZahlenderBetrag);
	     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	     	   
	     	   double eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	        }
	        double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	        
	        return rückgabebetrag;
	    }
	    
	    
	    public static void warte(int millisekunde) {
	    	
	    	System.out.println("\nFahrschein wird ausgegeben");
	    	
	    	 	for (int i = 0; i < 8; i++)
	        {
	    		System.out.print("=");
	           try {
	 			Thread.sleep(millisekunde);
	 		} catch (InterruptedException e) {
	 			e.printStackTrace();}
	        }
	        System.out.println("\n\n");
	    }
	    
	    
	    public static void rueckgeldAusgeben(double rückgabebetrag) {
	    	
	    	if(rückgabebetrag > 0.0)
	         {
	      	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro \n", rückgabebetrag);
	      	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	             while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	             {
	          	  System.out.println("2 EURO");
	  	          rückgabebetrag -= 2.0;
	             }
	             while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	             {
	          	  System.out.println("1 EURO");
	  	          rückgabebetrag -= 1.0;
	             }
	             while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	             {
	          	  System.out.println("50 CENT");
	  	          rückgabebetrag -= 0.5;
	             }
	             while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	             {
	          	  System.out.println("20 CENT");
	   	          rückgabebetrag -= 0.2;
	             }
	             while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	             {
	          	  System.out.println("10 CENT");
	  	          rückgabebetrag -= 0.1;
	  	          
	             }
	             while(Math.round(rückgabebetrag*100.0)/100.0 >= 0.05)// 5 CENT-Münzen
	             {
	          	  System.out.println("5 CENT");
	   	          rückgabebetrag -= 0.05;
	             }
	         }

	         System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                            "vor Fahrtantritt entwerten zu lassen!\n"+
	                            "Wir wünschen Ihnen eine gute Fahrt.");
	    }
	}