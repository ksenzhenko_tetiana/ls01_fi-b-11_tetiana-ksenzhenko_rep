//(0°C × 9/5) + 32 = 32°F

import java.util.Scanner;

public class Schleife2 {

	public static void main(String[] args) {
		Scanner ms = new Scanner(System.in);
		double i1, i2,f;
		int i;
		
		System.out.print("Bitte den Startwert in Celsius eingeben: ");
		i1 =  ms.nextDouble();
		
		System.out.print("\nBitte den Endwert in Celsius eingeben:  ");
		i2 =  ms.nextDouble();
		
		System.out.print("\nBitte die Schrittweite in Grad Celsius eingeben:  ");
        i =  ms.nextInt();
        
        while (i2 >= i1+i) {
            System.out.print(i1 + "°C");
            f = i1*9/5+32;
            i1=i1+i;
            System.out.println("     "+ f + "°F");
         }

   	}

}
